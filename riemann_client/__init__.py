"""A Python Riemann client and command line tool"""

__version__ = '6.1.3'
__author__ = 'Sam Clements <sam.clements@datasift.com>'

from .metrics import Gauge, RiemannClient, request_time
